# sparqlNegation

Short explanation on how to represent negation in SPARQL and possible use cases.

cf. https://www.w3.org/TR/sparql11-query/#negation


**NB:** with [Apache jena](https://jena.apache.org/), use the `--strict` option as a workaround for an [optimization bug](https://issues.apache.org/jira/browse/JENA-1963). 
See also [https://afs.github.io/substitute.html](https://afs.github.io/substitute.html) for a list of problems with `NOT EXISTS` and possible solutions.


## Todo

- [ ] persons who know someone who is not Alice (or Bob depending on previous examples). Both following examples are not equivalent: the first returns persons who do not know anyone, whereas the second only returns persons who know at least one person who is not Alice
    - [ ] `FILTER NOT EXISTS { ?p :knows :Alice } `
    - [ ] `FILTER ( ?friend != :Alice )`
